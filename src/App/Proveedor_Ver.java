/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;
//probando commits


import Coneccion.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author cajas
 */
public class Proveedor_Ver extends javax.swing.JFrame {
    PreparedStatement ps;

    /**
     * Creates new form Proveedor_Ver
     */
    public Proveedor_Ver() {
        initComponents();
        mostrar();
    }
    private void mostrar(){
        
        DefaultTableModel modelo=new DefaultTableModel();
        
        ResultSet rs= conexion.getTabla("Select CedulaRuc, Nombre, Direccion,Email,telefonop from proveedor ");
        modelo.setColumnIdentifiers(new Object[]{"cedulaRUC","nombre","direccion","email","telefono"});
        try {
            while(rs.next()){
                modelo.addRow(new Object[]{rs.getString("cedulaRuc"), rs.getString("nombre"), rs.getString("direccion"),rs.getString("email"),rs.getString("telefonop")});
            }
            TablaProveedorConsulta.setModel(modelo);
            
        } catch (Exception e) {
            System.out.println("error"+e);
        }
        
        
    }  
    //metodo para limpiar los TextArea
    private void limpiarCajas(){
        txtDireccion.setText(null);
        txtEmail.setText(null);
        txtNombre.setText(null);
        txtRuc.setText(null);
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnBuscar2 = new javax.swing.JButton();
        jColorChooser1 = new javax.swing.JColorChooser();
        BtnBuscar1 = new javax.swing.JButton();
        tituloproveedortxt = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtRuc = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaProveedorConsulta = new javax.swing.JTable();
        BtnAtras = new javax.swing.JButton();
        BtnBorrar = new javax.swing.JButton();
        BtnBuscar = new javax.swing.JButton();
        btnAñadir = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        TFproveedorNombre4 = new javax.swing.JTextField();
        BtnCambiar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        fondo = new javax.swing.JLabel();

        BtnBuscar2.setBackground(new java.awt.Color(0, 0, 0));
        BtnBuscar2.setText("Buscar");
        BtnBuscar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBuscar2ActionPerformed(evt);
            }
        });

        BtnBuscar1.setBackground(new java.awt.Color(0, 0, 0));
        BtnBuscar1.setText("Buscar");
        BtnBuscar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBuscar1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tituloproveedortxt.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 24)); // NOI18N
        tituloproveedortxt.setForeground(new java.awt.Color(0, 0, 0));
        tituloproveedortxt.setText("          PROVEEDOR");
        getContentPane().add(tituloproveedortxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 190, 24));

        jLabel1.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("RUC:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 50, -1, 24));
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(62, 378, -1, -1));

        txtRuc.setBackground(new java.awt.Color(255, 255, 255));
        txtRuc.setForeground(new java.awt.Color(0, 0, 0));
        txtRuc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRucActionPerformed(evt);
            }
        });
        getContentPane().add(txtRuc, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 50, 173, -1));

        TablaProveedorConsulta.setBackground(new java.awt.Color(255, 255, 255));
        TablaProveedorConsulta.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        TablaProveedorConsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "CedulaRuc", "Nombre", "Direccion", "Email", "telefono"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TablaProveedorConsulta.setColumnSelectionAllowed(true);
        TablaProveedorConsulta.setGridColor(new java.awt.Color(153, 255, 153));
        TablaProveedorConsulta.getTableHeader().setReorderingAllowed(false);
        TablaProveedorConsulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaProveedorConsultaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TablaProveedorConsulta);
        TablaProveedorConsulta.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (TablaProveedorConsulta.getColumnModel().getColumnCount() > 0) {
            TablaProveedorConsulta.getColumnModel().getColumn(0).setResizable(false);
            TablaProveedorConsulta.getColumnModel().getColumn(3).setResizable(false);
            TablaProveedorConsulta.getColumnModel().getColumn(4).setResizable(false);
        }

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 410, 250));

        BtnAtras.setBackground(new java.awt.Color(0, 0, 0));
        BtnAtras.setText("Atrás");
        BtnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAtrasActionPerformed(evt);
            }
        });
        getContentPane().add(BtnAtras, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 410, -1, -1));

        BtnBorrar.setBackground(new java.awt.Color(0, 0, 0));
        BtnBorrar.setText("Borrar");
        BtnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBorrarActionPerformed(evt);
            }
        });
        getContentPane().add(BtnBorrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 250, 70, -1));

        BtnBuscar.setBackground(new java.awt.Color(0, 0, 0));
        BtnBuscar.setText("Buscar");
        BtnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(BtnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 330, -1, -1));

        btnAñadir.setBackground(new java.awt.Color(0, 0, 0));
        btnAñadir.setText("Añadir");
        btnAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAñadirActionPerformed(evt);
            }
        });
        getContentPane().add(btnAñadir, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 410, 70, -1));

        jLabel3.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Nombre:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 80, -1, 24));

        jLabel4.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Direccion:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 120, -1, 24));

        jLabel5.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Telefono:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 200, -1, 24));

        jLabel6.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Por Nombre: ");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, 24));

        txtNombre.setBackground(new java.awt.Color(255, 255, 255));
        txtNombre.setForeground(new java.awt.Color(0, 0, 0));
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, 173, -1));

        txtDireccion.setBackground(new java.awt.Color(255, 255, 255));
        txtDireccion.setForeground(new java.awt.Color(0, 0, 0));
        txtDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDireccionActionPerformed(evt);
            }
        });
        getContentPane().add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, 173, -1));

        txtEmail.setBackground(new java.awt.Color(255, 255, 255));
        txtEmail.setForeground(new java.awt.Color(0, 0, 0));
        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });
        getContentPane().add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 160, 173, -1));

        TFproveedorNombre4.setBackground(new java.awt.Color(255, 255, 255));
        TFproveedorNombre4.setForeground(new java.awt.Color(0, 0, 0));
        TFproveedorNombre4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFproveedorNombre4ActionPerformed(evt);
            }
        });
        getContentPane().add(TFproveedorNombre4, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 330, 173, -1));

        BtnCambiar.setBackground(new java.awt.Color(0, 0, 0));
        BtnCambiar.setText("Cambiar");
        BtnCambiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCambiarActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCambiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 80, -1));

        jLabel7.setFont(new java.awt.Font("Franklin Gothic Medium Cond", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Email:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 160, -1, 24));

        txttelefono.setBackground(new java.awt.Color(255, 255, 255));
        txttelefono.setForeground(new java.awt.Color(0, 0, 0));
        txttelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttelefonoActionPerformed(evt);
            }
        });
        getContentPane().add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 200, 173, 20));

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fondo3.jpg"))); // NOI18N
        fondo.setText("jLabel3");
        getContentPane().add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 810, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBorrarActionPerformed
        // Metodo para borrar los datosrecibidos en los txt
        try {
            String campo=txtRuc.getText(); //obtengo el codigo unico de la tabla para localizarlo en la base
            String where="";
            if (!"".equals(campo)){
            where="WHERE cedulaRUC='"+campo+"'";
            }
            ps=conexion.getConexion().prepareStatement("DELETE FROM proveedor "+where);
            
           
            
            
            //execute
            
            int res=ps.executeUpdate();
            //compribacion
            if(res>0){
                JOptionPane.showMessageDialog(null, "Proveedor Borrado!!");
                
                mostrar();//reinicia la vista de la tabla
                limpiarCajas();//limpia los text
            }else{
                JOptionPane.showMessageDialog(null, "ERROR, no se pudo Borrar, contacte al Desarrollador!!");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }//GEN-LAST:event_BtnBorrarActionPerformed

    private void BtnBuscar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBuscar2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBuscar2ActionPerformed

    private void BtnBuscar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBuscar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnBuscar1ActionPerformed

    private void BtnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBuscarActionPerformed
        // usa el nombre ingresado lo busca y muestra e la jtable
        String campo= TFproveedorNombre4.getText();
        String where="";
        if (!"".equals(campo)){
            where="WHERE nombre='"+campo+"'";
        }
        
        DefaultTableModel modelo=new DefaultTableModel();
        String consult= "Select CedulaRuc, Nombre, Direccion,Email,telefonop from proveedor  "+where;
        System.out.println(consult);
        ResultSet rs= conexion.getTabla(consult);//encio la consulta al resultset
        
        modelo.setColumnIdentifiers(new Object[]{"cedulaRUC","nombre","direccion","email","telefono"});
        try {
            while(rs.next()){
                modelo.addRow(new Object[]{rs.getString("cedulaRuc"), rs.getString("nombre"), rs.getString("direccion"),rs.getString("email"),rs.getString("telefonop")});
            }
            TablaProveedorConsulta.setModel(modelo);
            
        } catch (Exception e) {
            System.out.println("error"+e);
        }
    }//GEN-LAST:event_BtnBuscarActionPerformed

    private void BtnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAtrasActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        MenuPrincipal menu=new MenuPrincipal();
        menu.setVisible(true);
    }//GEN-LAST:event_BtnAtrasActionPerformed

    private void btnAñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAñadirActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        Proveedor_Registrar pr= new Proveedor_Registrar();
        pr.setVisible(true);
    }//GEN-LAST:event_btnAñadirActionPerformed

    private void txtRucActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRucActionPerformed
        
    }//GEN-LAST:event_txtRucActionPerformed

    private void TablaProveedorConsultaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaProveedorConsultaMouseClicked
        // Script que rellena datos del crud al hacer click en la tabla:
        PreparedStatement ps=null;
        ResultSet rs=null;
        try {
            int Fila=TablaProveedorConsulta.getSelectedRow();
            String codigo=TablaProveedorConsulta.getValueAt( Fila,0).toString();
            
            ps=conexion.getConexion().prepareStatement("Select cedulaRUC,nombre,direccion,email,telefonop from proveedor WHERE cedulaRUC=?");
            ps.setString(1,codigo);
            rs=ps.executeQuery();
            
            while(rs.next()){
                txtRuc.setText(rs.getString("cedulaRUC"));
                txtDireccion.setText(rs.getString("direccion"));
                txtNombre.setText(rs.getString("nombre"));
                txtEmail.setText(rs.getString("email"));
                txttelefono.setText(rs.getString("telefonop"));
            
            }
            
            
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }//GEN-LAST:event_TablaProveedorConsultaMouseClicked

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDireccionActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailActionPerformed

    private void TFproveedorNombre4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFproveedorNombre4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFproveedorNombre4ActionPerformed

    private void BtnCambiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCambiarActionPerformed
        // TODO add your handling code here:
        try {
            String campo=txtRuc.getText(); //obtengo el codigo unico de la tabla para localizarlo en la base
            String where="";
            if (!"".equals(campo)){
            where="WHERE cedulaRUC='"+campo+"'";
            }
            ps=conexion.getConexion().prepareStatement("UPDATE  Proveedor SET nombre=?, direccion=?, email=?, telefonop=? "+where);
            
            ps.setString(1,txtNombre.getText());
            ps.setString(2,txtDireccion.getText());
            ps.setString(3,txtEmail.getText());
            ps.setString(4,txttelefono.getText());
            
            
            //execute
            
            int res=ps.executeUpdate();
            //compribacion
            if(res>0){
                JOptionPane.showMessageDialog(null, "Proveedor Actualizado!!");
                
                mostrar();//reinicia la vista de la tabla
                limpiarCajas();//limpia los text
            }else{
                JOptionPane.showMessageDialog(null, "ERROR, no se guardó el cambio, contacte al Desarrollador!!");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        
        
    }//GEN-LAST:event_BtnCambiarActionPerformed

    private void txttelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttelefonoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Proveedor_Ver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Proveedor_Ver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Proveedor_Ver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Proveedor_Ver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Proveedor_Ver().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAtras;
    private javax.swing.JButton BtnBorrar;
    private javax.swing.JButton BtnBuscar;
    private javax.swing.JButton BtnBuscar1;
    private javax.swing.JButton BtnBuscar2;
    private javax.swing.JButton BtnCambiar;
    private javax.swing.JTextField TFproveedorNombre4;
    private javax.swing.JTable TablaProveedorConsulta;
    private javax.swing.JButton btnAñadir;
    private javax.swing.JLabel fondo;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel tituloproveedortxt;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtRuc;
    private javax.swing.JTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
